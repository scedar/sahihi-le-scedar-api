package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "sls_courses", indexes = {
        @Index(name = "index_sls_courses_course_name", columnList = "course_name"),
        @Index(name = "index_sls_courses_updated", columnList = "updated"),
        @Index(name = "index_sls_courses_created", columnList = "created"),
        @Index(name = "index_sls_courses_active", columnList = "active")
},
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_sls_courses_course_code", columnNames = "course_code")
        }
)
@ApiModel(description = "This is a Course model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class Course {
    private int id;
    private String courseName;
    private String courseCode;
    private String courseDescription;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;
    private Set<UserAccount> courseLecturers;
    private Collection<Lecture> lecturesById;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "course_name", nullable = false, length = 150)
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Basic
    @Column(name = "course_code", nullable = false, length = 50)
    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    @Basic
    @Column(name = "course_description", nullable = true, length = 256)
    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (id != course.id) return false;
        if (active != course.active) return false;
        if (courseName != null ? !courseName.equals(course.courseName) : course.courseName != null) return false;
        if (courseCode != null ? !courseCode.equals(course.courseCode) : course.courseCode != null) return false;
        if (courseDescription != null ? !courseDescription.equals(course.courseDescription) : course.courseDescription != null)
            return false;
        if (created != null ? !created.equals(course.created) : course.created != null) return false;
        if (updated != null ? !updated.equals(course.updated) : course.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (courseName != null ? courseName.hashCode() : 0);
        result = 31 * result + (courseCode != null ? courseCode.hashCode() : 0);
        result = 31 * result + (courseDescription != null ? courseDescription.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "sls_course_lecturers",
            joinColumns = @JoinColumn(name = "sls_course_id"),
            inverseJoinColumns = @JoinColumn(name = "sls_user_account_id"))

    public Set<UserAccount> getCourseLecturers() {
        return courseLecturers;
    }

    public void setCourseLecturers(Set<UserAccount> courseLecturers) {
        this.courseLecturers = courseLecturers;
    }

    @OneToMany(mappedBy = "coursesByCourseId")
    public Collection<Lecture> getLecturesById() {
        return lecturesById;
    }

    public void setLecturesById(Collection<Lecture> lecturesById) {
        this.lecturesById = lecturesById;
    }
}
