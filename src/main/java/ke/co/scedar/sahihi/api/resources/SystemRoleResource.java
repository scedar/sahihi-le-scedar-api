package ke.co.scedar.sahihi.api.resources;

import ke.co.scedar.sahihi.api.controllers.SystemRoleController;
import ke.co.scedar.sahihi.api.domain.orm.SystemRole;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * sahihi-le-scedar-api (ke.co.scedar.sahihi.api.resources)
 * Created by: elon
 * On: 31 May, 2018 5/31/18 12:38 PM
 **/
public class SystemRoleResource extends ResourceSupport {

    private final SystemRole systemRole;

    public SystemRoleResource(SystemRole systemRole) {
        this.systemRole = systemRole;

        add(linkTo(methodOn(SystemRoleController.class).findAll()).withRel("roles"));
    }

    public SystemRole getSystemRole() {
        return systemRole;
    }
}
