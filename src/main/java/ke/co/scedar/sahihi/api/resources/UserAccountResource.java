package ke.co.scedar.sahihi.api.resources;

import ke.co.scedar.sahihi.api.controllers.UserAccountController;
import ke.co.scedar.sahihi.api.domain.orm.UserAccount;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * sahihi-le-scedar-api (ke.co.scedar.sahihi.api.resources)
 * Created by: elon
 * On: 31 May, 2018 5/31/18 12:14 PM
 **/
public class UserAccountResource extends ResourceSupport {

    private final UserAccount userAccount;


    public UserAccountResource(UserAccount userAccount) {
        this.userAccount = userAccount;
        add(linkTo(methodOn(UserAccountController.class).findAll()).withRel("users"));
        add(linkTo(methodOn(UserAccountController.class).rolesById(userAccount.getId())).withRel("roles"));
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }
}
