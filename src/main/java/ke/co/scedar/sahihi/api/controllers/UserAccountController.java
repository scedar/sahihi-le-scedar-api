package ke.co.scedar.sahihi.api.controllers;

import ke.co.scedar.sahihi.api.domain.orm.SystemRole;
import ke.co.scedar.sahihi.api.domain.orm.UserAccount;
import ke.co.scedar.sahihi.api.repositories.UserAccountRepository;
import ke.co.scedar.sahihi.api.resources.SystemRoleResource;
import ke.co.scedar.sahihi.api.resources.UserAccountResource;
import ke.co.scedar.sahihi.configurations.beans.NullAwareBeanUtils;
import ke.co.scedar.sahihi.configurations.exceptions.BusinessException;
import ke.co.scedar.sahihi.configurations.exceptions.ExceptionRepresentation;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * sahihi-le-scedar-api (ke.co.scedar.sahihi.api.controllers)
 * Created by: elon
 * On: 31 May, 2018 5/31/18 12:00 PM
 **/
@RestController
@RequestMapping(path = "/api",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class UserAccountController {

    private final UserAccountRepository userAccountRepository;
    private final NullAwareBeanUtils nullAwareBeanUtils;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserAccountController(UserAccountRepository userAccountRepository,
                                 NullAwareBeanUtils nullAwareBeanUtils,
                                 BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userAccountRepository = userAccountRepository;
        this.nullAwareBeanUtils = nullAwareBeanUtils;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping("/me")
    public Principal me(Principal principal){
        return principal;
    }

    @PreAuthorize("hasAnyRole('ROOT', 'ADMIN')")
    @GetMapping("/users")
    public ResponseEntity<Resources<UserAccountResource>>
    findAll(){
        List<UserAccountResource> collection =
                userAccountRepository
                        .findAll()
                        .stream()
                        .map(UserAccountResource::new)
                        .collect(Collectors.toList());
        final Resources<UserAccountResource> resources =
                new Resources<>(collection);
        final String uriString = ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUriString();
        resources.add(new Link(uriString, "self"));
        return ResponseEntity.ok(resources);
    }

    @GetMapping("/users/{id}")
    public UserAccountResource findById(@PathVariable Long id){
        Optional<UserAccount> userAccountOptional = userAccountRepository.findById(id);
        if (!userAccountOptional.isPresent())
            notFound(id);

        return new UserAccountResource(userAccountOptional.get());
    }

    @PreAuthorize("hasAnyRole('ROOT', 'ADMIN')")
    @PutMapping("/users")
    public ResponseEntity<Object> create(@Valid @RequestBody UserAccount user){
        UserAccount createdUser = userAccountRepository.save(user);

        URI resourceLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdUser.getId())
                .toUri();
        return ResponseEntity.created(resourceLocation).build();
    }

    @PatchMapping("/users")
    public ResponseEntity<Object> update(@RequestBody UserAccount userUpdates){

        Optional<UserAccount> optionalUserAccount =
                userAccountRepository.findById(userUpdates.getId());

        if(!optionalUserAccount.isPresent())
            notFound(userUpdates.getId());

        UserAccount user = optionalUserAccount.get();
        if(!(userUpdates.getUserPassword() == null || userUpdates.getUserPassword().equals("")))
            userUpdates.setUserPassword(bCryptPasswordEncoder.encode(userUpdates.getUserPassword()));

        try {
            nullAwareBeanUtils.copyProperties(user, userUpdates);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            String errors = "Error parsing properties for object "+UserAccount.class.getName()+
                    ","+e.toString();
            invalidObject("Error parsing update object",
                    new ArrayList<>(Arrays.asList(errors.split(","))));
        }

        UserAccount updatedUser = userAccountRepository.save(user);

        URI resourceLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(updatedUser.getId())
                .toUri();
        return ResponseEntity.created(resourceLocation).build();
    }

    @PreAuthorize("hasAnyRole('ROOT', 'ADMIN')")
    @DeleteMapping("/users/{id}")
    public void deleteById(@PathVariable Long id) {
        userAccountRepository.deleteById(id);
    }

    @GetMapping("/users/{id}/roles")
    public ResponseEntity<Resources<SystemRoleResource>>
    rolesById(@PathVariable Long id){
        Optional<UserAccount> userOptional = userAccountRepository.findById(id);
        if (!userOptional.isPresent())
            notFound(id);

        List<SystemRoleResource> collection = new ArrayList<>();
        Set<SystemRole> roles = userOptional.get().getUserRoles();

        for (SystemRole role : roles){
            collection.add(new SystemRoleResource(role));
        }

        Resources<SystemRoleResource> resources = new Resources<>(collection);
        final String uriString = ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUriString();
        resources.add(new Link(uriString, "self"));

        return ResponseEntity.ok(resources);
    }

    private void notFound(Long id){
        throw new BusinessException(
                new ExceptionRepresentation(
                        DateTimeUtils.getCurrentJavaUtilDateTime(),
                        HttpStatus.NOT_FOUND.value(),
                        "User with <id> {"+id+"} was not found",
                        Collections.singletonList("Resource not found"),
                        ServletUriComponentsBuilder
                                .fromCurrentRequestUri()
                                .toUriString()
                )
        );
    }

    private void invalidObject(String error, List<String> errors){
        throw new BusinessException(
                new ExceptionRepresentation(
                        DateTimeUtils.getCurrentJavaUtilDateTime(),
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        error,
                        errors,
                        ServletUriComponentsBuilder
                                .fromCurrentRequestUri()
                                .toUriString()
                )
        );
    }
}
