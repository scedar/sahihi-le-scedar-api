package ke.co.scedar.sahihi.api.controllers;

import ke.co.scedar.sahihi.api.domain.orm.SystemRole;
import ke.co.scedar.sahihi.api.repositories.SystemRoleRepository;
import ke.co.scedar.sahihi.api.resources.SystemRoleResource;
import ke.co.scedar.sahihi.configurations.exceptions.BusinessException;
import ke.co.scedar.sahihi.configurations.exceptions.ExceptionRepresentation;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * sahihi-le-scedar-api (ke.co.scedar.sahihi.api.controllers)
 * Created by: elon
 * On: 31 May, 2018 5/31/18 1:11 PM
 **/
@RestController
@PreAuthorize("hasAnyRole('ROOT', 'ADMIN')")
@RequestMapping(path = "/api",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class SystemRoleController {

    private final SystemRoleRepository systemRoleRepository;

    @Autowired
    public SystemRoleController(SystemRoleRepository systemRoleRepository) {
        this.systemRoleRepository = systemRoleRepository;
    }

    @GetMapping("/roles")
    public ResponseEntity<Resources<SystemRoleResource>>
    findAll(){
        List<SystemRoleResource> collection =
                systemRoleRepository
                        .findAll()
                        .stream()
                        .map(SystemRoleResource::new)
                        .collect(Collectors.toList());
        final Resources<SystemRoleResource> resources =
                new Resources<>(collection);
        final String uriString = ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUriString();
        resources.add(new Link(uriString, "self"));
        return ResponseEntity.ok(resources);
    }

    @GetMapping("/roles/{id}")
    public SystemRoleResource findById(@PathVariable Integer id){
        Optional<SystemRole> roleOptional = systemRoleRepository.findById(id);
        if (!roleOptional.isPresent())
            notFound(id);

        return new SystemRoleResource(roleOptional.get());
    }

    @PutMapping("/roles")
    public ResponseEntity<Object> create(@Valid @RequestBody SystemRole role){
        SystemRole createdRole = systemRoleRepository.save(role);

        URI resourceLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdRole.getId())
                .toUri();
        return ResponseEntity.created(resourceLocation).build();
    }

    @PatchMapping("/roles")
    public ResponseEntity<Object> update(@RequestBody SystemRole roleUpdates){

        Optional<SystemRole> roleOptional =
                systemRoleRepository.findById(roleUpdates.getId());

        if(!roleOptional.isPresent())
            notFound(roleUpdates.getId());

        SystemRole role = roleOptional.get();
        BeanUtils.copyProperties(roleUpdates, role);

        SystemRole updatedRole = systemRoleRepository.save(role);

        URI resourceLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(updatedRole.getId())
                .toUri();
        return ResponseEntity.created(resourceLocation).build();
    }

    @DeleteMapping("/roles/{id}")
    public void deleteById(@PathVariable Integer id) {
        systemRoleRepository.deleteById(id);
    }

    private void notFound(Integer id){
        throw new BusinessException(
                new ExceptionRepresentation(
                        DateTimeUtils.getCurrentJavaUtilDateTime(),
                        HttpStatus.NOT_FOUND.value(),
                        "Role with <id> {"+id+"} was not found",
                        Collections.singletonList("Resource not found"),
                        ServletUriComponentsBuilder
                                .fromCurrentRequestUri()
                                .toUriString()
                )
        );
    }
}
