package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "sls_user_types", indexes = {
        @Index(name = "index_sls_user_types_user_type", columnList = "user_type"),
        @Index(name = "index_sls_user_types_updated", columnList = "updated"),
        @Index(name = "index_sls_user_types_created", columnList = "created"),
        @Index(name = "index_sls_user_types_active", columnList = "active")
},
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_sls_user_types_finger_type", columnNames = "user_type")
        }
)
@ApiModel(description = "This is a UserType model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class UserType {
    private int id;
    private String userType;
    private String userTypeDescription;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;
    private Collection<UserAccount> userAccountsById;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_type", nullable = false, length = 150)
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Basic
    @Column(name = "user_type_description", nullable = true, length = 256)
    public String getUserTypeDescription() {
        return userTypeDescription;
    }

    public void setUserTypeDescription(String userTypeDescription) {
        this.userTypeDescription = userTypeDescription;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserType userType1 = (UserType) o;

        if (id != userType1.id) return false;
        if (active != userType1.active) return false;
        if (userType != null ? !userType.equals(userType1.userType) : userType1.userType != null) return false;
        if (userTypeDescription != null ? !userTypeDescription.equals(userType1.userTypeDescription) : userType1.userTypeDescription != null)
            return false;
        if (created != null ? !created.equals(userType1.created) : userType1.created != null) return false;
        if (updated != null ? !updated.equals(userType1.updated) : userType1.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (userType != null ? userType.hashCode() : 0);
        result = 31 * result + (userTypeDescription != null ? userTypeDescription.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @OneToMany(mappedBy = "userTypesByUserTypeId")
    public Collection<UserAccount> getUserAccountsById() {
        return userAccountsById;
    }

    public void setUserAccountsById(Collection<UserAccount> userAccountsById) {
        this.userAccountsById = userAccountsById;
    }
}
