package ke.co.scedar.sahihi.api.repositories;

import ke.co.scedar.sahihi.api.domain.orm.SystemRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@RepositoryRestResource(exported = false)
@Transactional
public interface SystemRoleRepository extends JpaRepository<SystemRole, Integer> {

    List<SystemRole> findAll();

    List<SystemRole> findByActive(boolean active);

    Optional<SystemRole> findById(Integer id);

    Optional<SystemRole> findByIdAndActive(Integer id, boolean active);

    Optional<SystemRole> findByRoleName(String roleName);

    void deleteById(Integer id);

}
