package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "sls_lecturer_attendance_status", indexes = {
        @Index(name = "index_sls_lecturer_attendance_status_lecturer_attendance_status", columnList = "lecturer_attendance_status"),
        @Index(name = "index_sls_lecturer_attendance_status_updated", columnList = "updated"),
        @Index(name = "index_sls_lecturer_attendance_status_created", columnList = "created"),
        @Index(name = "index_sls_lecturer_attendance_status_active", columnList = "active")
},
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_sls_lecturer_attendance_status_lecturer_attendance_status", columnNames = "lecturer_attendance_status")
        }
)
@ApiModel(description = "This is a LecturerAttendanceStatus model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class LecturerAttendanceStatus {
    private int id;
    private String lecturerAttendanceStatus;
    private String lecturerAttendanceStatusDesc;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;
    private Collection<Lecture> lecturesById;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "lecturer_attendance_status", nullable = false, length = 150)
    public String getLecturerAttendanceStatus() {
        return lecturerAttendanceStatus;
    }

    public void setLecturerAttendanceStatus(String lecturerAttendanceStatus) {
        this.lecturerAttendanceStatus = lecturerAttendanceStatus;
    }

    @Basic
    @Column(name = "lecturer_attendance_status_desc", nullable = true, length = 256)
    public String getLecturerAttendanceStatusDesc() {
        return lecturerAttendanceStatusDesc;
    }

    public void setLecturerAttendanceStatusDesc(String lecturerAttendanceStatusDesc) {
        this.lecturerAttendanceStatusDesc = lecturerAttendanceStatusDesc;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LecturerAttendanceStatus that = (LecturerAttendanceStatus) o;

        if (id != that.id) return false;
        if (active != that.active) return false;
        if (lecturerAttendanceStatus != null ? !lecturerAttendanceStatus.equals(that.lecturerAttendanceStatus) : that.lecturerAttendanceStatus != null)
            return false;
        if (lecturerAttendanceStatusDesc != null ? !lecturerAttendanceStatusDesc.equals(that.lecturerAttendanceStatusDesc) : that.lecturerAttendanceStatusDesc != null)
            return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (lecturerAttendanceStatus != null ? lecturerAttendanceStatus.hashCode() : 0);
        result = 31 * result + (lecturerAttendanceStatusDesc != null ? lecturerAttendanceStatusDesc.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @OneToMany(mappedBy = "lecturerAttendanceStatusByLecturerAttendanceStatusId")
    public Collection<Lecture> getLecturesById() {
        return lecturesById;
    }

    public void setLecturesById(Collection<Lecture> lecturesById) {
        this.lecturesById = lecturesById;
    }
}
