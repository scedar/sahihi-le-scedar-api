package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Table(name = "sls_lectures", indexes = {
        @Index(name = "index_sls_lectures_course_id", columnList = "sls_course_id"),
        @Index(name = "index_sls_lectures_start_time", columnList = "start_time"),
        @Index(name = "index_sls_lectures_end_time", columnList = "end_time"),
        @Index(name = "index_sls_lectures_date", columnList = "date"),
        @Index(name = "index_sls_lectures_lecturer_attendance_status_id", columnList = "sls_lecturer_attendance_status_id"),
        @Index(name = "index_sls_lectures_updated", columnList = "updated"),
        @Index(name = "index_sls_lectures_created", columnList = "created"),
        @Index(name = "index_sls_lectures_active", columnList = "active")
})
@ApiModel(description = "This is a Lecture model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class Lecture {
    private long id;
    private int courseId;
    private Time startTime;
    private Time endTime;
    private Date date;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;
    private int lecturerAttendanceStatusId;
    private Course coursesByCourseId;
    private LecturerAttendanceStatus lecturerAttendanceStatusByLecturerAttendanceStatusId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sls_course_id", nullable = false)
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = false)
    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "date", nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "sls_lecturer_attendance_status_id", nullable = false)
    public int getLecturerAttendanceStatusId() {
        return lecturerAttendanceStatusId;
    }

    public void setLecturerAttendanceStatusId(int lecturerAttendanceStatusId) {
        this.lecturerAttendanceStatusId = lecturerAttendanceStatusId;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lecture lecture = (Lecture) o;

        if (id != lecture.id) return false;
        if (courseId != lecture.courseId) return false;
        if (active != lecture.active) return false;
        if (lecturerAttendanceStatusId != lecture.lecturerAttendanceStatusId) return false;
        if (startTime != null ? !startTime.equals(lecture.startTime) : lecture.startTime != null) return false;
        if (endTime != null ? !endTime.equals(lecture.endTime) : lecture.endTime != null) return false;
        if (date != null ? !date.equals(lecture.date) : lecture.date != null) return false;
        if (created != null ? !created.equals(lecture.created) : lecture.created != null) return false;
        if (updated != null ? !updated.equals(lecture.updated) : lecture.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + courseId;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        result = 31 * result + lecturerAttendanceStatusId;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "sls_course_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public Course getCoursesByCourseId() {
        return coursesByCourseId;
    }

    public void setCoursesByCourseId(Course coursesByCourseId) {
        this.coursesByCourseId = coursesByCourseId;
    }

    @ManyToOne
    @JoinColumn(name = "sls_lecturer_attendance_status_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public LecturerAttendanceStatus getLecturerAttendanceStatusByLecturerAttendanceStatusId() {
        return lecturerAttendanceStatusByLecturerAttendanceStatusId;
    }

    public void setLecturerAttendanceStatusByLecturerAttendanceStatusId(LecturerAttendanceStatus lecturerAttendanceStatusByLecturerAttendanceStatusId) {
        this.lecturerAttendanceStatusByLecturerAttendanceStatusId = lecturerAttendanceStatusByLecturerAttendanceStatusId;
    }
}
