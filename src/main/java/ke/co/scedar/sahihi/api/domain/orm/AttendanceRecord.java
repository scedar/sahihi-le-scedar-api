package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Table(name = "sls_attendance_records", indexes = {
        @Index(name = "index_sls_attendance_records_course_code", columnList = "course_code"),
        @Index(name = "index_sls_attendance_records_lecturer_attendance_status", columnList = "lecturer_attendance_status"),
        @Index(name = "index_sls_attendance_records_lecture_date", columnList = "lecture_date"),
        @Index(name = "index_sls_attendance_records_lecture_start_time", columnList = "lecture_start_time"),
        @Index(name = "index_sls_attendance_records_lecture_end_time", columnList = "lecture_end_time"),
        @Index(name = "index_sls_attendance_records_student_user_id", columnList = "student_user_id"),
        @Index(name = "index_sls_attendance_records_student_first_name", columnList = "student_first_name"),
        @Index(name = "index_sls_attendance_records_student_last_name", columnList = "student_last_name"),
        @Index(name = "index_sls_attendance_records_student_email", columnList = "student_email"),
        @Index(name = "index_sls_attendance_records_student_phone_number", columnList = "student_phone_number"),
        @Index(name = "index_sls_attendance_records_student_username", columnList = "student_username"),
        @Index(name = "index_sls_attendance_records_student_date_of_birth", columnList = "student_date_of_birth"),
        @Index(name = "index_sls_attendance_records_lecture_id", columnList = "lecture_id")
})
@ApiModel(description = "This is a AttendanceRecord model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class AttendanceRecord {
    private long id;
    private String courseCode;
    private String lecturerAttendanceStatus;
    private Date lectureDate;
    private Time lectureStartTime;
    private Time lectureEndTime;
    private String studentUserId;
    private String studentFirstName;
    private String studentLastName;
    private String studentEmail;
    private String studentPhoneNumber;
    private String studentUsername;
    private Timestamp studentDateOfBirth;
    private long lectureId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "course_code", nullable = false, length = 50)
    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    @Basic
    @Column(name = "lecturer_attendance_status", nullable = false, length = 150)
    public String getLecturerAttendanceStatus() {
        return lecturerAttendanceStatus;
    }

    public void setLecturerAttendanceStatus(String lecturerAttendanceStatus) {
        this.lecturerAttendanceStatus = lecturerAttendanceStatus;
    }

    @Basic
    @Column(name = "lecture_date", nullable = false)
    public Date getLectureDate() {
        return lectureDate;
    }

    public void setLectureDate(Date lectureDate) {
        this.lectureDate = lectureDate;
    }

    @Basic
    @Column(name = "lecture_start_time", nullable = false)
    public Time getLectureStartTime() {
        return lectureStartTime;
    }

    public void setLectureStartTime(Time lectureStartTime) {
        this.lectureStartTime = lectureStartTime;
    }

    @Basic
    @Column(name = "lecture_end_time", nullable = false)
    public Time getLectureEndTime() {
        return lectureEndTime;
    }

    public void setLectureEndTime(Time lectureEndTime) {
        this.lectureEndTime = lectureEndTime;
    }

    @Basic
    @Column(name = "student_user_id", nullable = false, length = 150)
    public String getStudentUserId() {
        return studentUserId;
    }

    public void setStudentUserId(String studentUserId) {
        this.studentUserId = studentUserId;
    }

    @Basic
    @Column(name = "student_first_name", nullable = false, length = 256)
    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    @Basic
    @Column(name = "student_last_name", nullable = false, length = 256)
    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    @Basic
    @Column(name = "student_email", nullable = true, length = 256)
    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    @Basic
    @Column(name = "student_phone_number", nullable = true, length = 50)
    public String getStudentPhoneNumber() {
        return studentPhoneNumber;
    }

    public void setStudentPhoneNumber(String studentPhoneNumber) {
        this.studentPhoneNumber = studentPhoneNumber;
    }

    @Basic
    @Column(name = "student_username", nullable = false, length = 150)
    public String getStudentUsername() {
        return studentUsername;
    }

    public void setStudentUsername(String studentUsername) {
        this.studentUsername = studentUsername;
    }

    @Basic
    @Column(name = "student_date_of_birth", nullable = false)
    public Timestamp getStudentDateOfBirth() {
        return studentDateOfBirth;
    }

    public void setStudentDateOfBirth(Timestamp studentDateOfBirth) {
        this.studentDateOfBirth = studentDateOfBirth;
    }

    @Basic
    @Column(name = "lecture_id", nullable = false)
    public long getLectureId() {
        return lectureId;
    }

    public void setLectureId(long lectureId) {
        this.lectureId = lectureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttendanceRecord that = (AttendanceRecord) o;

        if (id != that.id) return false;
        if (lectureId != that.lectureId) return false;
        if (courseCode != null ? !courseCode.equals(that.courseCode) : that.courseCode != null) return false;
        if (lecturerAttendanceStatus != null ? !lecturerAttendanceStatus.equals(that.lecturerAttendanceStatus) : that.lecturerAttendanceStatus != null)
            return false;
        if (lectureDate != null ? !lectureDate.equals(that.lectureDate) : that.lectureDate != null) return false;
        if (lectureStartTime != null ? !lectureStartTime.equals(that.lectureStartTime) : that.lectureStartTime != null)
            return false;
        if (lectureEndTime != null ? !lectureEndTime.equals(that.lectureEndTime) : that.lectureEndTime != null)
            return false;
        if (studentUserId != null ? !studentUserId.equals(that.studentUserId) : that.studentUserId != null)
            return false;
        if (studentFirstName != null ? !studentFirstName.equals(that.studentFirstName) : that.studentFirstName != null)
            return false;
        if (studentLastName != null ? !studentLastName.equals(that.studentLastName) : that.studentLastName != null)
            return false;
        if (studentEmail != null ? !studentEmail.equals(that.studentEmail) : that.studentEmail != null) return false;
        if (studentPhoneNumber != null ? !studentPhoneNumber.equals(that.studentPhoneNumber) : that.studentPhoneNumber != null)
            return false;
        if (studentUsername != null ? !studentUsername.equals(that.studentUsername) : that.studentUsername != null)
            return false;
        if (studentDateOfBirth != null ? !studentDateOfBirth.equals(that.studentDateOfBirth) : that.studentDateOfBirth != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (courseCode != null ? courseCode.hashCode() : 0);
        result = 31 * result + (lecturerAttendanceStatus != null ? lecturerAttendanceStatus.hashCode() : 0);
        result = 31 * result + (lectureDate != null ? lectureDate.hashCode() : 0);
        result = 31 * result + (lectureStartTime != null ? lectureStartTime.hashCode() : 0);
        result = 31 * result + (lectureEndTime != null ? lectureEndTime.hashCode() : 0);
        result = 31 * result + (studentUserId != null ? studentUserId.hashCode() : 0);
        result = 31 * result + (studentFirstName != null ? studentFirstName.hashCode() : 0);
        result = 31 * result + (studentLastName != null ? studentLastName.hashCode() : 0);
        result = 31 * result + (studentEmail != null ? studentEmail.hashCode() : 0);
        result = 31 * result + (studentPhoneNumber != null ? studentPhoneNumber.hashCode() : 0);
        result = 31 * result + (studentUsername != null ? studentUsername.hashCode() : 0);
        result = 31 * result + (studentDateOfBirth != null ? studentDateOfBirth.hashCode() : 0);
        result = 31 * result + (int) (lectureId ^ (lectureId >>> 32));
        return result;
    }
}
