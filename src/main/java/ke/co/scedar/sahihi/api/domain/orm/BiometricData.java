package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;

@Entity
@Table(name = "sls_biometric_data", indexes = {
        @Index(name = "index_sls_biometric_data_user_account_id", columnList = "sls_user_account_id"),
        @Index(name = "index_sls_biometric_data_finger_type_id", columnList = "finger_type_id"),
        @Index(name = "index_sls_biometric_data_finger_print_data", columnList = "finger_print_data"),
        @Index(name = "index_sls_biometric_data_updated", columnList = "updated"),
        @Index(name = "index_sls_biometric_data_created", columnList = "created"),
        @Index(name = "index_sls_biometric_data_active", columnList = "active")
},
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_sls_biometric_data_finger_print_data", columnNames = "finger_print_data")
        }
)
@ApiModel(description = "This is a BiometricData model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class BiometricData {
    private long id;
    private long userAccountId;
    private int fingerTypeId;
    private byte[] fingerPrintData;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;
    private UserAccount userAccountsByUserAccountId;
    private FingerType fingerTypesByFingerTypeId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sls_user_account_id", nullable = false)
    public long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(long userAccountId) {
        this.userAccountId = userAccountId;
    }

    @Basic
    @Column(name = "finger_type_id", nullable = false)
    public int getFingerTypeId() {
        return fingerTypeId;
    }

    public void setFingerTypeId(int fingerTypeId) {
        this.fingerTypeId = fingerTypeId;
    }

    @Basic
    @Column(name = "finger_print_data", nullable = false)
    public byte[] getFingerPrintData() {
        return fingerPrintData;
    }

    public void setFingerPrintData(byte[] fingerPrintData) {
        this.fingerPrintData = fingerPrintData;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BiometricData that = (BiometricData) o;

        if (id != that.id) return false;
        if (userAccountId != that.userAccountId) return false;
        if (fingerTypeId != that.fingerTypeId) return false;
        if (active != that.active) return false;
        if (!Arrays.equals(fingerPrintData, that.fingerPrintData)) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (userAccountId ^ (userAccountId >>> 32));
        result = 31 * result + fingerTypeId;
        result = 31 * result + Arrays.hashCode(fingerPrintData);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "sls_user_account_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public UserAccount getUserAccountsByUserAccountId() {
        return userAccountsByUserAccountId;
    }

    public void setUserAccountsByUserAccountId(UserAccount userAccountsByUserAccountId) {
        this.userAccountsByUserAccountId = userAccountsByUserAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "finger_type_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public FingerType getFingerTypesByFingerTypeId() {
        return fingerTypesByFingerTypeId;
    }

    public void setFingerTypesByFingerTypeId(FingerType fingerTypesByFingerTypeId) {
        this.fingerTypesByFingerTypeId = fingerTypesByFingerTypeId;
    }
}
