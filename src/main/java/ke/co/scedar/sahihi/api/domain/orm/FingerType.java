package ke.co.scedar.sahihi.api.domain.orm;

import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "sls_finger_types", indexes = {
        @Index(name = "index_sls_finger_types_finger_type", columnList = "finger_type"),
        @Index(name = "index_sls_finger_types_updated", columnList = "updated"),
        @Index(name = "index_sls_finger_types_created", columnList = "created"),
        @Index(name = "index_sls_finger_types_active", columnList = "active")
},
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_sls_finger_types_finger_type", columnNames = "finger_type")
        }
)
@ApiModel(description = "This is a FingerType model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class FingerType {
    private int id;
    private String fingerType;
    private String fingerTypeDescription;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;
    private Collection<BiometricData> biometricDataById;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "finger_type", nullable = false, length = 150)
    public String getFingerType() {
        return fingerType;
    }

    public void setFingerType(String fingerType) {
        this.fingerType = fingerType;
    }

    @Basic
    @Column(name = "finger_type_description", nullable = true, length = 256)
    public String getFingerTypeDescription() {
        return fingerTypeDescription;
    }

    public void setFingerTypeDescription(String fingerTypeDescription) {
        this.fingerTypeDescription = fingerTypeDescription;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FingerType that = (FingerType) o;

        if (id != that.id) return false;
        if (active != that.active) return false;
        if (fingerType != null ? !fingerType.equals(that.fingerType) : that.fingerType != null) return false;
        if (fingerTypeDescription != null ? !fingerTypeDescription.equals(that.fingerTypeDescription) : that.fingerTypeDescription != null)
            return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fingerType != null ? fingerType.hashCode() : 0);
        result = 31 * result + (fingerTypeDescription != null ? fingerTypeDescription.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @OneToMany(mappedBy = "fingerTypesByFingerTypeId")
    public Collection<BiometricData> getBiometricDataById() {
        return biometricDataById;
    }

    public void setBiometricDataById(Collection<BiometricData> biometricDataById) {
        this.biometricDataById = biometricDataById;
    }
}
