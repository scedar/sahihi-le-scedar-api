package ke.co.scedar.sahihi.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * sahihi-le-scedar-api (ke.co.scedar.sahihi.api.controllers)
 * Created by: elon
 * On: 31 May, 2018 5/31/18 1:32 PM
 **/
@RestController
public class SahihiErrorController implements ErrorController {

    public class Error {

        public Integer status;
        public String error;
        public String message;
        public String timeStamp;
        public String trace;

        public Error(int status, Map<String, Object> errorAttributes) {
            this.status = status;
            this.error = (String) errorAttributes.get("error");
            this.message = (String) errorAttributes.get("message");
            this.timeStamp = errorAttributes.get("timestamp").toString();
            this.trace = (String) errorAttributes.get("trace");
        }

    }

    private static final String PATH = "/error";

    private boolean debug = false;

    private final ErrorAttributes errorAttributes;

    @Autowired
    public SahihiErrorController(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }

    @RequestMapping(value = PATH)
    Error error(HttpServletRequest request, HttpServletResponse response) {
        String d = request.getParameter("debug");
        if(!(d == null || d.equals("")) && d.equals("true")) debug = true;
        Error error = new Error(response.getStatus(), getErrorAttributes(request, debug));
        debug = false;

        return error;
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        ServletWebRequest servletWebRequest = new ServletWebRequest(request);
        return errorAttributes.getErrorAttributes(servletWebRequest, includeStackTrace);
    }

}