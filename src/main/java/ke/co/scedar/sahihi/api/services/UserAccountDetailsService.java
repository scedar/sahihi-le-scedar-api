package ke.co.scedar.sahihi.api.services;

import ke.co.scedar.sahihi.api.domain.UserAccountDetails;
import ke.co.scedar.sahihi.api.domain.orm.UserAccount;
import ke.co.scedar.sahihi.api.repositories.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserAccountDetailsService implements UserDetailsService {

    private final UserAccountRepository userAccountRepository;

    @Autowired
    public UserAccountDetailsService(UserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        //Using username
        Optional<UserAccount> optionalUserAccount =
                userAccountRepository.findByUserNameAndActive(s, true);

        if(!optionalUserAccount.isPresent()){

            //Use userId
            optionalUserAccount =
                    userAccountRepository.findByUserIdAndActive(s, true);

            optionalUserAccount
                    .orElseThrow(() -> new UsernameNotFoundException("Username or User ID not found"));
        }

        return optionalUserAccount.map(UserAccountDetails::new).get();
    }
}
