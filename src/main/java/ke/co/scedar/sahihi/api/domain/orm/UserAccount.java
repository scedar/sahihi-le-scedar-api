package ke.co.scedar.sahihi.api.domain.orm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import ke.co.scedar.sahihi.security.ScedarUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "sls_user_accounts", indexes = {
        @Index(name = "index_sls_user_accounts_username", columnList = "username"),
        @Index(name = "index_sls_user_accounts_user_id", columnList = "user_id"),
        @Index(name = "index_sls_user_accounts_email_address", columnList = "email_address"),
        @Index(name = "index_sls_user_accounts_user_type_id", columnList = "user_type_id"),
        @Index(name = "index_sls_user_accounts_updated", columnList = "updated"),
        @Index(name = "index_sls_user_accounts_created", columnList = "created"),
        @Index(name = "index_sls_user_accounts_active", columnList = "active")
},
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_sls_user_accounts_lecturer_attendance_username", columnNames = "username"),
                @UniqueConstraint(name = "unique_sls_user_accounts_lecturer_attendance_user_id", columnNames = "user_id")
        }
)
@ApiModel(description = "This is a UserAccount model")
@DynamicUpdate
@DynamicInsert
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "EntityCache")
public class UserAccount {
    private long id;
    private String firstName;
    private String lastName;
    private String otherNames;
    private String userPassword;
    private Timestamp dateOfBirth;
    private String userName;
    private String emailAddress;
    private String phoneNumber;
    private String residenceAddress;
    private String bio;
    private Integer userTypeId;
    private String userId;
    private Timestamp created;
    private Timestamp updated;
    private boolean active;

    private Set<SystemRole> userRoles;
    private Collection<BiometricData> biometricDataById;
    private UserType userTypesByUserTypeId;

    public UserAccount(){

    }

    public UserAccount(UserAccount userAccount){
        this.id = userAccount.getId();
        this.firstName = userAccount.getFirstName();
        this.lastName = userAccount.getLastName();
        this.otherNames = userAccount.getOtherNames();
        this.userPassword = userAccount.getUserPassword();
        this.userRoles = userAccount.getUserRoles();
        this.dateOfBirth = userAccount.getDateOfBirth();
        this.userName = userAccount.getUserName();
        this.emailAddress = userAccount.getEmailAddress();
        this.phoneNumber = userAccount.getPhoneNumber();
        this.residenceAddress = userAccount.getResidenceAddress();
        this.bio = userAccount.getBio();
        this.userTypeId = userAccount.getUserTypeId();
        this.userId = userAccount.getUserId();
        this.created = userAccount.getCreated();
        this.updated = userAccount.getUpdated();
        this.active = userAccount.isActive();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 150)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 150)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "other_names", nullable = true, length = 256)
    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    @JsonIgnore
    @Basic
    @Column(name = "user_password", nullable = true, length = 256)
    public String getUserPassword() {
        return userPassword;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Basic
    @Column(name = "date_of_birth", nullable = false)
    public Timestamp getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Timestamp dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 150)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "email_address", nullable = true, length = 256)
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Basic
    @Column(name = "phone_number", nullable = true, length = 50)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Lob
    @Column(name = "residence_address", nullable = true)
    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    @Basic
    @Lob
    @Column(name = "bio", nullable = true)
    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @Basic
    @Column(name = "user_type_id", nullable = true)
    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Basic
    @Column(name = "user_id", nullable = true, length = 50)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @PrePersist
    void preInsert(){
        this.created = DateTimeUtils.getCurrentSqlTimestamp();
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
        this.active = true;
    }

    @PreUpdate
    void preUpdate(){
        this.updated = DateTimeUtils.getCurrentSqlTimestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAccount that = (UserAccount) o;

        if (id != that.id) return false;
        if (active != that.active) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (otherNames != null ? !otherNames.equals(that.otherNames) : that.otherNames != null) return false;
        if (dateOfBirth != null ? !dateOfBirth.equals(that.dateOfBirth) : that.dateOfBirth != null) return false;
        if (userPassword != null ? !userPassword.equals(that.userPassword) : that.userPassword != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (emailAddress != null ? !emailAddress.equals(that.emailAddress) : that.emailAddress != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (residenceAddress != null ? !residenceAddress.equals(that.residenceAddress) : that.residenceAddress != null)
            return false;
        if (bio != null ? !bio.equals(that.bio) : that.bio != null) return false;
        if (userTypeId != null ? !userTypeId.equals(that.userTypeId) : that.userTypeId != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (otherNames != null ? otherNames.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        result = 31 * result + (userPassword != null ? userPassword.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (emailAddress != null ? emailAddress.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (residenceAddress != null ? residenceAddress.hashCode() : 0);
        result = 31 * result + (bio != null ? bio.hashCode() : 0);
        result = 31 * result + (userTypeId != null ? userTypeId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "sls_user_roles",
            joinColumns = @JoinColumn(name = "sls_user_account_id"),
            inverseJoinColumns = @JoinColumn(name = "sls_system_role_id"))
    public Set<SystemRole> getUserRoles() {
        return userRoles;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public void setUserRoles(Set<SystemRole> userRoles) {
        this.userRoles = userRoles;
    }

    public String getUserRoles(boolean print) {
        StringBuilder stringBuilder = new StringBuilder();

        for (SystemRole systemRole: userRoles){
            stringBuilder.append(" Role ID: ")
                    .append(systemRole.getId())
                    .append("; Role Name: ")
                    .append(systemRole.getRoleName());
        }

        return stringBuilder.toString();
    }

    @JsonIgnore
    @OneToMany(mappedBy = "userAccountsByUserAccountId")
    public Collection<BiometricData> getBiometricDataById() {
        return biometricDataById;
    }

    public void setBiometricDataById(Collection<BiometricData> biometricDataById) {
        this.biometricDataById = biometricDataById;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_type_id", referencedColumnName = "id", insertable = false, updatable = false)
    public UserType getUserTypesByUserTypeId() {
        return userTypesByUserTypeId;
    }

    public void setUserTypesByUserTypeId(UserType userTypesByUserTypeId) {
        this.userTypesByUserTypeId = userTypesByUserTypeId;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", otherNames='" + otherNames + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", userName='" + userName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", residenceAddress='" + residenceAddress + '\'' +
                ", bio='" + bio + '\'' +
                ", userTypeId=" + userTypeId +
                ", userId='" + userId + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", active=" + active +
                ", userRoles=" + userRoles +
                '}';
    }
}
