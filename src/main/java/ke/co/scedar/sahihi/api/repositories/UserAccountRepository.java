package ke.co.scedar.sahihi.api.repositories;

import ke.co.scedar.sahihi.api.domain.orm.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@RepositoryRestResource(exported = false)
@Transactional
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    Optional<UserAccount> findById(Long id);

    Optional<UserAccount> findByIdAndActive(Long id, boolean active);

    Optional<UserAccount> findByUserNameAndActive(String username, boolean active);

    Optional<UserAccount> findByUserIdAndActive(String userId, boolean active);

    List<UserAccount> findAll();

    List<UserAccount> findByActive(boolean active);

    void deleteById(Long id);

}
