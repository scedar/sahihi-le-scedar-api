package ke.co.scedar.sahihi.utils;

import ke.co.scedar.sahihi.configurations.DatabaseSetup;
import ke.co.scedar.sahihi.security.ScedarEncryption;
import ke.co.scedar.sahihi.utils.file_utils.XmlUtils;

import java.util.Properties;

public class ConstantsUtils {

    public static DatabaseSetup databaseSetup;

    public static Properties getApplicationProperties(){

        databaseSetup = new DatabaseSetup();

        Properties properties = new Properties();
        properties.put("server.port", ConstantsUtils.getApiContextPort());
        properties.put("server.servlet.context-path", ConstantsUtils.getApiContextPath());

        properties.put("spring.jpa.hibernate.ddl-auto", ConstantsUtils.getHibHbm2ddlAuto(true));
        properties.put("spring.datasource.url", databaseSetup.getConnectionUrl());
        properties.put("spring.datasource.driver-class-name", databaseSetup.getDatabaseDriver());
        properties.put("spring.datasource.username", ConstantsUtils.getDbUsername());
        properties.put("spring.datasource.password", ConstantsUtils.getDbPassword());

        properties.put("spring.jpa.properties.hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory");
        properties.put("spring.jpa.properties.hibernate.connection.driver_class", databaseSetup.getDatabaseDriver());
        properties.put("spring.jpa.properties.hibernate.dialect", databaseSetup.getDatabaseDialect());
        properties.put("spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults", ConstantsUtils.getHibUseJdbcMetadataDefaults());
        properties.put("spring.jpa.properties.hibernate.default_schema", ConstantsUtils.getHibDefaultSchema());
        properties.put("spring.jpa.properties.hibernate.default_catalog", ConstantsUtils.getHibDefaultCatalog());
        properties.put("spring.jpa.properties.hibernate.format_sql", ConstantsUtils.getHibFormatSql());
        properties.put("spring.jpa.properties.hibernate.cache.use_query_cache", ConstantsUtils.getHibUseQueryCache());
        properties.put("spring.jpa.properties.hibernate.cache.use_second_level_cache", ConstantsUtils.getHibUseSecondLevelCache());
        properties.put("spring.jpa.properties.hibernate.generate_statistics", ConstantsUtils.getHibGenerateStatistics());
        properties.put("spring.jpa.properties.hibernate.c3p0.min_size", ConstantsUtils.getHibC3p0MinSize());
        properties.put("spring.jpa.properties.hibernate.c3p0.max_size", ConstantsUtils.getHibC3p0MaxSize());
        properties.put("spring.jpa.properties.hibernate.c3p0.timeout", ConstantsUtils.getHibC3p0Timeout());
        properties.put("spring.jpa.properties.hibernate.c3p0.max_statements", ConstantsUtils.getHibC3p0MaxStatements());
        properties.put("spring.jpa.properties.hibernate.c3p0.idle_test_period", ConstantsUtils.getHibC3p0IdleTestPeriod());
        properties.put("spring.jpa.properties.connection.pool_size", ConstantsUtils.getHibConnPoolSize());
        properties.put("spring.jpa.properties.hibernate.current_session_context_class", "thread");
        properties.put("spring.jpa.properties.hibernate.c3p0.acquire_increment", "3");
        properties.put("spring.jpa.properties.hibernate.c3p0.contextClassLoaderSource", "library");
        properties.put("spring.jpa.properties.hibernate.cache.provider_configuration_file_resource_path", "classpath:ehcache.xml");

        properties.put("spring.jackson.time-zone", getDefaultTimeZone());
        properties.put("spring.jackson.serialization.write-dates-as-timestamps", true);
        properties.put("management.endpoints.web.exposure.include", "*");
        properties.put("management.endpoint.shutdown.enabled", true);
        properties.put("spring.messages.basename", "messages");

        return properties;
    }

    /*=======================================================================================*/
    /*                              CONFIG FILE VARS & DEFAULTS                              */
    /*=======================================================================================*/
    public static final String mySql = "MySQL";
    public static final String microsoftSql = "MicrosoftSQL";
    public static final String oracle = "Oracle";
    public static final String postgreSql = "PostgreSQL";

    /*=======================================================================================*/
    /*                              DATE TIME                                                */
    /*=======================================================================================*/

    //Date Time Formats
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    // Just Now Desc.
    public static final String JUST_NOW = "Just now";

    /*=======================================================================================*/
    /*                              MISCELLANEOUS                                            */
    /*=======================================================================================*/
    public static final String ENCRYPTED = "ENCRYPTED";
    public static final String CLEAR_TEXT = "CLEAR_TEXT";
    public static final String SKY_DELIMITER = "&s*k@y{$d}$&";

    public static final String LOCAL_CONF_FILENAME = "conf.xml";

    public static final String XML_PATH_TO_CONTEXT_PATH = "/API/CONTEXT/PATH";
    public static final String XML_PATH_TO_CONTEXT_PORT = "/API/CONTEXT/PORT";
    public static final String XML_PATH_TO_CONTEXT_ENABLE_BASIC_AUTH = "/API/CONTEXT/ENABLE_BASIC_AUTH";

    public static final String XML_PATH_TO_CONTEXT_TIME_ZONE = "/API/CONTEXT/TIME_ZONE";

    public static final String XML_PATH_TO_UNDERTOW_ENABLE_ACCESS_LOG = "/API/CONTEXT/UNDERTOW/ENABLE_ACCESS_LOG";
    public static final String XML_PATH_TO_UNDERTOW_ACCESS_LOG_DIR = "/API/CONTEXT/UNDERTOW/ACCESS_LOG_DIR";
    public static final String XML_PATH_TO_UNDERTOW_ACCESS_LOG_PATTERN = "/API/CONTEXT/UNDERTOW/ACCESS_LOG_PATTERN";
    public static final String XML_PATH_TO_UNDERTOW_ENABLE_COMPRESSION = "/API/CONTEXT/UNDERTOW/ENABLE_COMPRESSION";
    public static final String XML_PATH_TO_UNDERTOW_COMPRESSION_MIN_RESPONSE_SIZE = "/API/CONTEXT/UNDERTOW/COMPRESSION_MIN_RESPONSE_SIZE";

    private static final String XML_PATH_TO_SERVER= "/API/DB/SERVER";
    private static final String XML_PATH_TO_HOST = "/API/DB/HOST";
    private static final String XML_PATH_TO_PORT = "/API/DB/PORT";
    private static final String XML_PATH_TO_DATABASE_NAME = "/API/DB/NAME";
    private static final String XML_PATH_TO_DB_PASSWORD_TYPE = "/API/DB/PASSWORD_TYPE";
    private static final String XML_PATH_TO_DB_USERNAME = "/API/DB/USERNAME";
    private static final String XML_PATH_TO_DB_PASSWORD = "/API/DB/PASSWORD";
    private static final String XML_PATH_TO_DB_INITIALIZE = "/API/DB/INITIALIZE";

    private static final String XML_PATH_TO_HIBERNATE_HBM2DDL_AUTO = "/API/DB/HIBERNATE/HBM2DDL_AUTO";
    private static final String XML_PATH_TO_HIBERNATE_DEFAULT_SCHEMA = "/API/DB/HIBERNATE/DEFAULT_SCHEMA";
    private static final String XML_PATH_TO_HIBERNATE_DEFAULT_CATALOG = "/API/DB/HIBERNATE/DEFAULT_CATALOG";
    private static final String XML_PATH_TO_HIBERNATE_USE_JDBC_METADATA_DEFAULTS = "/API/DB/HIBERNATE/USE_JDBC_METADATA_DEFAULTS";
    private static final String XML_PATH_TO_HIBERNATE_CONNECTION_POOL_SIZE = "/API/DB/HIBERNATE/CONNECTION_POOL_SIZE";
    private static final String XML_PATH_TO_HIBERNATE_SHOW_SQL = "/API/DB/HIBERNATE/SHOW_SQL";
    private static final String XML_PATH_TO_HIBERNATE_FORMAT_SQL = "/API/DB/HIBERNATE/FORMAT_SQL";
    private static final String XML_PATH_TO_HIBERNATE_USE_SECOND_LEVEL_CACHE = "/API/DB/HIBERNATE/USE_SECOND_LEVEL_CACHE";
    private static final String XML_PATH_TO_HIBERNATE_USE_QUERY_CACHE = "/API/DB/HIBERNATE/USE_QUERY_CACHE";
    private static final String XML_PATH_TO_HIBERNATE_GENERATE_STATISTICS = "/API/DB/HIBERNATE/GENERATE_STATISTICS";
    private static final String XML_PATH_TO_HIBERNATE_C3P0_MIN_SIZE = "/API/DB/HIBERNATE/C3P0/MIN_SIZE";
    private static final String XML_PATH_TO_HIBERNATE_C3P0_MAX_SIZE = "/API/DB/HIBERNATE/C3P0/MAX_SIZE";
    private static final String XML_PATH_TO_HIBERNATE_C3P0_TIMEOUT = "/API/DB/HIBERNATE/C3P0/TIMEOUT";
    private static final String XML_PATH_TO_HIBERNATE_C3P0_MAX_STATEMENTS = "/API/DB/HIBERNATE/C3P0/MAX_STATEMENTS";
    private static final String XML_PATH_TO_HIBERNATE_C3P0_IDLE_TEST_PERIOD = "/API/DB/HIBERNATE/C3P0/IDLE_TEST_PERIOD";

    private static final String XML_PATH_TO_TEMP_FILE_PATH = "/API/PATHS/TMPFILE";

    public static String getEnableBasicAuth(){
        return XmlUtils.readXMLTag(XML_PATH_TO_CONTEXT_ENABLE_BASIC_AUTH);
    }

    public static String getDefaultTimeZone(){
        return XmlUtils.readXMLTag(XML_PATH_TO_CONTEXT_TIME_ZONE);
    }


    public static String getUndertowEnableAccessLog(){
        return XmlUtils.readXMLTag(XML_PATH_TO_UNDERTOW_ENABLE_ACCESS_LOG);
    }
    public static String getUndertowAccessLogDir(){
        return XmlUtils.readXMLTag(XML_PATH_TO_UNDERTOW_ACCESS_LOG_DIR);
    }
    public static String getUndertowAccessLogPattern(){
        return XmlUtils.readXMLTag(XML_PATH_TO_UNDERTOW_ACCESS_LOG_PATTERN);
    }
    public static String getUndertowEnableCompression(){
        return XmlUtils.readXMLTag(XML_PATH_TO_UNDERTOW_ENABLE_COMPRESSION);
    }
    public static String getUndertowCompressionMinResponseSize(){
        return XmlUtils.readXMLTag(XML_PATH_TO_UNDERTOW_COMPRESSION_MIN_RESPONSE_SIZE);
    }


    public static String getDbServer() {
        return XmlUtils.readXMLTag(XML_PATH_TO_SERVER);
    }

    public static String getApiContextPath() {
        return XmlUtils.readXMLTag(XML_PATH_TO_CONTEXT_PATH);
    }
    public static Integer getApiContextPort() {
        return Integer.parseInt(XmlUtils.readXMLTag(XML_PATH_TO_CONTEXT_PORT));
    }

    public static String getDbHost() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HOST);
    }

    public static String getDbPort() {
        return XmlUtils.readXMLTag(XML_PATH_TO_PORT);
    }

    public static String getDbName() {
        return XmlUtils.readXMLTag(XML_PATH_TO_DATABASE_NAME);
    }

    public static String getHibShowSql() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_SHOW_SQL);
    }

    public static String getHibFormatSql() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_FORMAT_SQL);
    }

    public static String getHibUseQueryCache() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_USE_QUERY_CACHE);
    }

    public static String getHibGenerateStatistics() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_GENERATE_STATISTICS);
    }

    public static String getHibUseSecondLevelCache() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_USE_SECOND_LEVEL_CACHE);
    }

    public static String getHibHbm2ddlAuto(boolean refactor) {

        String hbm2dllAuto = XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_HBM2DDL_AUTO).toLowerCase();
        System.out.println(hbm2dllAuto);

        if(refactor) if(hbm2dllAuto.equals("create") || hbm2dllAuto.equals("update"))
            XmlUtils.updateXMLTag(XML_PATH_TO_HIBERNATE_HBM2DDL_AUTO, "validate");

        return hbm2dllAuto;
    }

    public static String getHibHbm2ddlAuto() {
        return getHibHbm2ddlAuto(false);
    }

    public static String getHibDefaultSchema() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_DEFAULT_SCHEMA);
    }

    public static String getHibDefaultCatalog() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_DEFAULT_CATALOG);
    }

    public static String getHibUseJdbcMetadataDefaults() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_USE_JDBC_METADATA_DEFAULTS);
    }

    public static String getHibConnPoolSize() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_CONNECTION_POOL_SIZE);
    }

    public static String getHibC3p0MinSize() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_C3P0_MIN_SIZE);
    }

    public static String getHibC3p0MaxSize() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_C3P0_MAX_SIZE);
    }

    public static String getHibC3p0Timeout() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_C3P0_TIMEOUT);
    }

    public static String getHibC3p0MaxStatements() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_C3P0_MAX_STATEMENTS);
    }

    public static String getHibC3p0IdleTestPeriod() {
        return XmlUtils.readXMLTag(XML_PATH_TO_HIBERNATE_C3P0_IDLE_TEST_PERIOD);
    }

    public static String getPathToTmpFile() {
        return XmlUtils.readXMLTag(XML_PATH_TO_TEMP_FILE_PATH);
    }

    public static String getPasswordType() {

        String passwordType = XmlUtils.readXMLTag(XML_PATH_TO_DB_PASSWORD_TYPE);

        if(!passwordType.equals(SKY_DELIMITER)){
            return passwordType;
        }

        return SKY_DELIMITER;
    }

    public static String getDbUsername(){

        String dbUsername = XmlUtils.readXMLTag(XML_PATH_TO_DB_USERNAME);
        System.out.println("DB Username: "+dbUsername);

        if (!dbUsername.equals(SKY_DELIMITER)){
            return dbUsername;
        }
        return SKY_DELIMITER;
    }

    public static String getDbPassword(){
        if (getPasswordType().equals(CLEAR_TEXT)){
            String dbPassword = XmlUtils.readXMLTag(XML_PATH_TO_DB_PASSWORD);
            System.out.println("DB PWD"+dbPassword);

            String encryptedText = ScedarEncryption.encrypt(dbPassword);

            if (!(encryptedText.equals(SKY_DELIMITER) || dbPassword.equals(SKY_DELIMITER))){
                setDbPassword(encryptedText);
                setDbPasswordType(ENCRYPTED);
                return dbPassword;
            }

            return SKY_DELIMITER;
        }else{
            String decryptedText = ScedarEncryption.decrypt(
                    XmlUtils.readXMLTag(XML_PATH_TO_DB_PASSWORD)
            );

            if (!decryptedText.equals(SKY_DELIMITER)){
                return decryptedText;
            }
        }
        return SKY_DELIMITER;
    }

    public static boolean doWeInitializeDb(){
        String dbInitStatus = XmlUtils.readXMLTag(XML_PATH_TO_DB_INITIALIZE);

        if(dbInitStatus.equals("yes")){
            XmlUtils.updateXMLTag(XML_PATH_TO_DB_INITIALIZE, "no");
            return true;
        }

        return false;
    }

    private static void setDbPasswordType(String credentialsType)  {
        XmlUtils.updateXMLTag(XML_PATH_TO_DB_PASSWORD_TYPE, credentialsType);
    }

    public static boolean setDbUsername(String dbUsedname)  {
        return XmlUtils.updateXMLTag(XML_PATH_TO_DB_USERNAME, dbUsedname);
    }

    private static void setDbPassword(String dbPassword)  {
        XmlUtils.updateXMLTag(XML_PATH_TO_DB_PASSWORD, dbPassword);
    }

}
