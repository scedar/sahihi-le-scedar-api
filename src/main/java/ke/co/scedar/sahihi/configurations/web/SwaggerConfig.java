package ke.co.scedar.sahihi.configurations.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private Contact contact = new Contact(
           "William Ochomo",
            "https://scedar.co.ke",
            "ochomoswill@gmail.com"
    );

    private ApiInfo apiInfo = new ApiInfo(
            "Sahihi le Scedar Api Documentation",
            "API Docs for Sahihi le Scedar",
            "0.1.0.DEBUG",
            "https://scedar.co.ke/tos/apis/sahihi",
            contact,
            "Scedar Tech Inc. 0.3",
            "https://scedar.co.ke/licenses/LICENSE-0.3");
    private Set<String> producesAndCondumes =
            new HashSet<>(Arrays.asList("application/json", "application/xml"));

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .produces(producesAndCondumes);
    }

}
