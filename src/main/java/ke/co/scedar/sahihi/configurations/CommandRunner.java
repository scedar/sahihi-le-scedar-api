package ke.co.scedar.sahihi.configurations;

import ke.co.scedar.sahihi.api.domain.orm.SystemRole;
import ke.co.scedar.sahihi.api.domain.orm.UserAccount;
import ke.co.scedar.sahihi.api.repositories.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;

import static ke.co.scedar.sahihi.utils.ConstantsUtils.doWeInitializeDb;
import static ke.co.scedar.sahihi.utils.DateTimeUtils.getCurrentSqlTimestamp;

@Component
public class CommandRunner implements CommandLineRunner {

    private final UserAccountRepository userAccountRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public CommandRunner(UserAccountRepository userAccountRepository,
                         BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userAccountRepository = userAccountRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void run(String...args) {

        if(doWeInitializeDb()){
            UserAccount rootAccount = setupSystemUserAccount();

            if(rootAccount != null){
                System.out.println("ROOT Username -> "+rootAccount.getUserName());
                System.out.println("ROOT User ID -> "+rootAccount.getUserId());
                System.out.println("ROOT Password -> SahihiAdmin1234");
                System.out.println("ROOT ROLES -> "+rootAccount.getUserRoles(true));
            }else{
                System.out.println("ROOT Account not created. An error occurred / account already exists");
            }
        }
    }

    private UserAccount setupSystemUserAccount(){

        try{
            HashSet<SystemRole> roles = new HashSet<>();
            roles.add(new SystemRole("USER", "This is s basic / standard user role"));
            roles.add(new SystemRole("ADMIN", "This is the system administrator role"));
            roles.add(new SystemRole("ROOT", "This is the system ROOT role"));

            UserAccount userAccount = new UserAccount();
            userAccount.setFirstName("Root");
            userAccount.setLastName("Toor");
            userAccount.setUserPassword(
                    bCryptPasswordEncoder.encode("SahihiAdmin1234"));
            userAccount.setDateOfBirth(getCurrentSqlTimestamp());
            userAccount.setUserName("root@sahihi");
            userAccount.setUserId("root.sahihi@scedar.co.ke");
            userAccount.setUserRoles(roles);


            return userAccountRepository.save(userAccount);
        }catch (Exception e){
            System.out.println(e.toString());
        }

        return null;
    }
}
