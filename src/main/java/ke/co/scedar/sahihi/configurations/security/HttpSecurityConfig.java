package ke.co.scedar.sahihi.configurations.security;

import ke.co.scedar.sahihi.api.repositories.UserAccountRepository;
import ke.co.scedar.sahihi.api.services.UserAccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@EnableJpaRepositories(basePackageClasses = UserAccountRepository.class)
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class HttpSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserAccountDetailsService userAccountDetailsService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public HttpSecurityConfig(UserAccountDetailsService userAccountDetailsService,
                              BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userAccountDetailsService = userAccountDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userAccountDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .anonymous().disable()
                .authorizeRequests()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/browser/**").authenticated()
                .anyRequest()
                .permitAll().and()
                .httpBasic().and()
                .formLogin().permitAll();
    }
}
