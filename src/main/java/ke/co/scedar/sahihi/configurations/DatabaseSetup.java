package ke.co.scedar.sahihi.configurations;

import ke.co.scedar.sahihi.utils.ConstantsUtils;

import javax.xml.bind.ValidationException;

public class DatabaseSetup {

    /*
    create role vbams with login;
    alter role vbams with password 'Vbams0$';
    create database vbams;
    grant all on DATABASE vbams to vbams;
    alter database vbams owner to vbams;
     */

    private String connectionUrl;
    private String databaseDriver;
    private String databaseDialect;

    public DatabaseSetup(){
        try {
            init();
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void init() throws ValidationException {
        String databaseServer = ConstantsUtils.getDbServer();
        System.out.println(databaseServer);

        switch (databaseServer){
            case ConstantsUtils.mySql:
                setupMySql();
                break;
            case ConstantsUtils.postgreSql:
                setupPostgreSql();
                break;
            case ConstantsUtils.microsoftSql:
                setupMicrosoftSql();
                break;
            case ConstantsUtils.oracle:
                setupOracle();
                break;
            default:
                throw new ValidationException("Error: Unable to determine Database Server");

        }

    }

    private void setupMySql(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            databaseDriver = "com.mysql.cj.jdbc.Driver";
            System.out.println("MySQL Driver located :)");

            try{
                Class.forName("org.hibernate.dialect.MySQL57InnoDBDialect");
                databaseDialect = "org.hibernate.dialect.MySQL57InnoDBDialect";
                System.out.println("MySQL Database dialect dependencies located...");

                connectionUrl = "jdbc:mysql://"+ConstantsUtils.getDbHost()+":"+ConstantsUtils.getDbPort()+
                        "/"+ConstantsUtils.getDbName()+"?useSSL=false&createDatabaseIfNotExist=true";
                System.out.println("Connection URL -> "+connectionUrl);

            }catch (ClassNotFoundException e){
                System.err.print("Error. MySQL Database dialect dependencies found!");
            }

        } catch( ClassNotFoundException e ) {
            System.err.print("Error. MySQL Driver not found!");
        }
    }

    private void setupPostgreSql(){
        try {
            Class.forName("org.postgresql.Driver");
            databaseDriver = "org.postgresql.Driver";
            System.out.println("PostgreSQL Driver located :)");

            try{
                Class.forName("org.hibernate.dialect.PostgreSQL82Dialect");
                databaseDialect = "org.hibernate.dialect.PostgreSQL82Dialect";
                System.out.println("PostgreSQL Database dialect dependencies located...");

                connectionUrl = "jdbc:postgresql://"+ConstantsUtils.getDbHost()+":"
                        +ConstantsUtils.getDbPort()+"/"+ConstantsUtils.getDbName();

            }catch (ClassNotFoundException e){
                System.err.print("Error. PostgreSQL Database dialect dependencies found!");
            }

        } catch( ClassNotFoundException e ) {
            System.err.print("Error. PostgreSQL Driver not found!");
        }
    }

    private void setupMicrosoftSql(){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            databaseDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            System.out.println("MicrosoftSQL Driver located :)");

            try{
                Class.forName("org.hibernate.dialect.SQLServerDialect");
                databaseDialect = "org.hibernate.dialect.SQLServerDialect";
                System.out.println("MicrosoftSQL Database dialect dependencies located...");

                connectionUrl = "jdbc:sqlserver://"+ConstantsUtils.getDbHost()+":"+ConstantsUtils.getDbPort()+
                        ";databaseName="+ConstantsUtils.getDbName();
                System.out.println("Connection URL -> "+connectionUrl);

            }catch (ClassNotFoundException e){
                System.err.print("Error. MicrosoftSQL Database dialect dependencies found!");
            }

        } catch( ClassNotFoundException e ) {
            System.err.print("Error. MicrosoftSQL Driver not found!");
        }
    }

    private void setupOracle(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            databaseDriver = "com.mysql.jdbc.Driver";
            System.out.println("Oracle Driver located :)");

            try{
                Class.forName("org.hibernate.dialect.MySQL57InnoDBDialect");
                databaseDialect = "org.hibernate.dialect.MySQL57InnoDBDialect";
                System.out.println("Oracle Database dialect dependencies located...");

                connectionUrl = "jdbc:mysql://"+ConstantsUtils.getDbHost()+":"+ConstantsUtils.getDbPort()+
                        "/"+ConstantsUtils.getDbName()+"?useSSL=false&amp;createDatabaseIfNotExist=true";
                System.out.println("Connection URL -> "+connectionUrl);

            }catch (ClassNotFoundException e){
                System.err.print("Error. Oracle Database dialect dependencies found!");
            }

        } catch( ClassNotFoundException e ) {
            System.err.print("Error. Oracle Driver not found!");
        }
    }

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public String getDatabaseDriver() {
        return databaseDriver;
    }

    public String getDatabaseDialect() {
        return databaseDialect;
    }
}
