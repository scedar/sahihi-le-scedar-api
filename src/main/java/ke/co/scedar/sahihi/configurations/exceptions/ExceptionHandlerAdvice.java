package ke.co.scedar.sahihi.configurations.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@RestController
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    @ResponseBody
    ResponseEntity<Object> handleBusiness(BusinessException exception) {

        ExceptionRepresentation body = exception.getException();

        HttpStatus responseStatus = HttpStatus.valueOf(exception.getException().getStatus());
        return new ResponseEntity<>(body, responseStatus);
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity<Object> handleMultiple(BusinessExceptions exception) {

        List<ExceptionRepresentation> body = exception.getExceptions();
        HttpStatus responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(body, responseStatus);

    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        ArrayList<String> message = new ArrayList<>();

        for (ObjectError error : ex.getBindingResult().getAllErrors()){
            message.add(error.getDefaultMessage());
        }

        ExceptionRepresentation body = new ExceptionRepresentation(
                message,
                ex.getBindingResult().toString()
        );
        body.setPath(request.getContextPath());
        body.setStatus(HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}
