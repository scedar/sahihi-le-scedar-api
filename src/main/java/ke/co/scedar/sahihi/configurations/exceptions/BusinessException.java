package ke.co.scedar.sahihi.configurations.exceptions;

import java.util.Collections;

public class BusinessException extends RuntimeException {

    private ExceptionRepresentation exception = null;

    public BusinessException(String message) {
        super(message);
        this.exception = new ExceptionRepresentation(Collections.singletonList(message));
    }

    public BusinessException(ExceptionRepresentation exception) {
        this.exception = exception;
    }

    public ExceptionRepresentation getException() {
        return exception;
    }

    public void setException(ExceptionRepresentation exception) {
        this.exception = exception;
    }

    @Override
    public String toString() {
        return "BusinessException{" +
                " exception=" + exception +
                '}';
    }
}
