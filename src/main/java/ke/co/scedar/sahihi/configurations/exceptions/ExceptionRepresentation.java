package ke.co.scedar.sahihi.configurations.exceptions;

import ke.co.scedar.sahihi.security.ScedarUUID;
import ke.co.scedar.sahihi.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExceptionRepresentation {

    private String id;
    private Date timestamp;
    private Integer status;
    private String error;
    private List<String> message;
    private String path;

    public ExceptionRepresentation() {
        this.id = new ScedarUUID().getScedarUUID();
        this.timestamp = DateTimeUtils.getCurrentJavaUtilDateTime();
        this.status = 500;
        this.error = "Unknown Server Error";
        this.message = new ArrayList<>();
        this.path = "/";
    }

    public ExceptionRepresentation(List<String> message){
        this.id = new ScedarUUID().getScedarUUID();
        this.timestamp = DateTimeUtils.getCurrentJavaUtilDateTime();
        this.status = 500;
        this.error = "Unknown Server Error";
        this.message = message;
        this.path = "/";
    }

    public ExceptionRepresentation(List<String> message, String error){
        this.id = new ScedarUUID().getScedarUUID();
        this.timestamp = DateTimeUtils.getCurrentJavaUtilDateTime();
        this.status = 500;
        this.error = error;
        this.message = message;
        this.path = "/";
    }

    public ExceptionRepresentation(Date timestamp,
                                   Integer status,
                                   String error,
                                   List<String> message,
                                   String path) {
        this.id = new ScedarUUID().getScedarUUID();
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ExceptionRepresentation{" +
                "timestamp=" + timestamp +
                ", status=" + status +
                ", error='" + error + '\'' +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
