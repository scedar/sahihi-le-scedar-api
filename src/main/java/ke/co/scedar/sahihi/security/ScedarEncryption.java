package ke.co.scedar.sahihi.security;

import ke.co.scedar.sahihi.utils.ConstantsUtils;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Objects;

public class ScedarEncryption {

    private static final String UNICODE_FORMAT = "UTF8";
    private static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private Cipher cipher;
    private SecretKey key;

    private ScedarEncryption(){
        String encryptionKey = "ThisIsElonThisIsElonThisIsElon";
        String encryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        byte[] arrayBytes = new byte[0];
        try {
            arrayBytes = encryptionKey.getBytes(UNICODE_FORMAT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        KeySpec ks = null;
        try {
            ks = new DESedeKeySpec(arrayBytes);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        SecretKeyFactory skf = null;
        try {
            skf = SecretKeyFactory.getInstance(encryptionScheme);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            cipher = Cipher.getInstance(encryptionScheme);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            key = Objects.requireNonNull(skf).generateSecret(ks);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    private ScedarEncryption(String encryptionKey){
        String encryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        byte[] arrayBytes = new byte[0];
        try {
            arrayBytes = encryptionKey.getBytes(UNICODE_FORMAT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        KeySpec ks = null;
        try {
            ks = new DESedeKeySpec(arrayBytes);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        SecretKeyFactory skf = null;
        try {
            skf = SecretKeyFactory.getInstance(encryptionScheme);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            cipher = Cipher.getInstance(encryptionScheme);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            key = Objects.requireNonNull(skf).generateSecret(ks);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    private ScedarEncryption(String encryptionKey, String encryptionScheme){
        byte[] arrayBytes = new byte[0];
        try {
            arrayBytes = encryptionKey.getBytes(UNICODE_FORMAT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        KeySpec ks = null;
        try {
            ks = new DESedeKeySpec(arrayBytes);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        SecretKeyFactory skf = null;
        try {
            skf = SecretKeyFactory.getInstance(encryptionScheme);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            cipher = Cipher.getInstance(encryptionScheme);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            key = Objects.requireNonNull(skf).generateSecret(ks);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String unencryptedString) {
        String encryptedString = null;
        ScedarEncryption scedarEncryption = new ScedarEncryption();
        try {
            scedarEncryption.cipher.init(Cipher.ENCRYPT_MODE, scedarEncryption.key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = scedarEncryption.cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
            return ConstantsUtils.SKY_DELIMITER;
        }
        return encryptedString;
    }


    public static String decrypt(String encryptedString) {
        String decryptedText=null;
        ScedarEncryption scedarEncryption = new ScedarEncryption();
        try {
            scedarEncryption.cipher.init(Cipher.DECRYPT_MODE, scedarEncryption.key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = scedarEncryption.cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
            return ConstantsUtils.SKY_DELIMITER;
        }
        return decryptedText;
    }

}
