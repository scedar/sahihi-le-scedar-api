package ke.co.scedar.sahihi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

import static ke.co.scedar.sahihi.utils.ConstantsUtils.getApplicationProperties;
import static ke.co.scedar.sahihi.utils.ConstantsUtils.getDefaultTimeZone;

@SpringBootApplication
public class SahihiApiApplication {

    @PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone(getDefaultTimeZone()));
    }

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SahihiApiApplication.class);
        application.setDefaultProperties(getApplicationProperties());
        application.run(args);
    }
}
