# Sahihi le Scedar
## sahihi-le-scedar-api

Sahihi le Scedar (SlS) 
is a university lecture attendance monitoring system

## System Requirements
| # | Requirement |
|---|-------------|
| i.| Linux / Windows Server with at least 8 GB RAM, 250 GB HDD, 2 x 2.8 GHZ Processor. (Recommended - Linux, Debian based, 16 GB RAM, 512 GB HDD, 4 x 3.2 GHZ Processor|
| ii.|PostgreSQL 9.6 or later / MySQL 5.7 or later / SQL Server 2012 or later / Oracle Database 10g or later (Recommended - PostgreSQL 10.3 (Debian 10.3-1.pgdg90+1))|
| iii.|Oracle Java SE Development Kit 8u171 (Explicitly)|
| iv.|Apache Maven 3 or later (Recommended - 3.5.2)
| v.|NGINX 1.8.1 or later (Optional as reverse proxy for Wildfly but recommended)|


## Database Setup
> Note: We will use PostgreSQL in this guide. Though any of the listed databases will do

- Log into the database server and create a database called **sahihi** 
`postgres=# create database sahihi;`
- Now create a role named **sahihi** with login rights
`postgres=# create role sahihi with login;`
- Next, give **sahihi** role a password by running
`postgres=# alter role sahihi with password 'Sahihi2018$'`
- Then grant **sahihi** appropriate rights to **sahihi** database by running the following
    - `postgres=# alter database sahihi owner to saihi;`
    - `postgres=# grant all on database sahihi to sahihi;`
- With that, the database part is all set up

## Setup dev environment
> Note: This section assumes that you have already installed the earlier stated requirements

- Clone the repository via `git clone https://bitbucket.org/scedar/sahihi-le-scedar-api.git` into a dir of choice. I'll assume `~`.
- Now `-$ cd ~/sahihi-le-scedar-api`
- Make sure you have the following folder structure and `lib/ojdbc14.jar` jar file
```bash
~
├── README.md
├── conf.xml
├── lib
│   └── ojdbc14.jar
├── mvnw
├── mvnw.cmd
├── pom.xml
└──src/
```
- After you've verified that, run `-$ mvn install:install-file -Dfile=~/sahihi-le-scedar-api/lib/ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.5 -Dpackaging=jar`
- After this, go to the **config** file `conf.xml`
    - Here, set appropriate database details.
    - For the above database setup, the **config** would look like so
    ```xml
      <API>
          <CONTEXT>
              <PORT>8080</PORT>
              <PATH>/</PATH>
              <TIME_ZONE>Africa/Nairobi</TIME_ZONE>
              <ENABLE_BASIC_AUTH>true</ENABLE_BASIC_AUTH>
              <UNDERTOW>
                  <ENABLE_ACCESS_LOG>true</ENABLE_ACCESS_LOG>
                  <ACCESS_LOG_DIR>/media/elon/code/Java/projects/uon/sahihi-le-scedar-api/temp/</ACCESS_LOG_DIR>
                  <ACCESS_LOG_PATTERN>combined</ACCESS_LOG_PATTERN>
                  <ENABLE_COMPRESSION>true</ENABLE_COMPRESSION>
                  <COMPRESSION_MIN_RESPONSE_SIZE>1</COMPRESSION_MIN_RESPONSE_SIZE>
              </UNDERTOW>
          </CONTEXT>
          <DB>
              <SERVER>PostgreSQL</SERVER>
              <HOST>localhost</HOST>
              <PORT>5432</PORT>
              <NAME>sahihi</NAME>
              <USERNAME>sahihi</USERNAME>
              <PASSWORD>Sahihi2018$</PASSWORD>
              <PASSWORD_TYPE>CLEAR_TEXT</PASSWORD_TYPE>   <!--ENCRYPTED / CLEAR_TEXT -->
              <INITIALIZE>yes</INITIALIZE>
              <HIBERNATE>
                  <HBM2DDL_AUTO>create</HBM2DDL_AUTO> <!-- create / update / validate (Default -> validate)-->
                  <DEFAULT_SCHEMA>public</DEFAULT_SCHEMA>
                  <DEFAULT_CATALOG></DEFAULT_CATALOG>
                  <USE_JDBC_METADATA_DEFAULTS>false</USE_JDBC_METADATA_DEFAULTS>
                  <CONNECTION_POOL_SIZE>500</CONNECTION_POOL_SIZE>
                  <SHOW_SQL>true</SHOW_SQL>   <!-- true / false (Default -> true)-->
                  <FORMAT_SQL>true</FORMAT_SQL>   <!-- true / false (Default -> true)-->
                  <USE_SECOND_LEVEL_CACHE>true</USE_SECOND_LEVEL_CACHE>    <!-- true / false (Default -> true)-->
                  <USE_QUERY_CACHE>true</USE_QUERY_CACHE>   <!-- true / false (Default -> true)-->
                  <GENERATE_STATISTICS>false</GENERATE_STATISTICS>   <!-- true / false (Default -> false)-->
                  <C3P0>
                      <MIN_SIZE>5</MIN_SIZE> <!-- Min number of JDBC connections in the pool -->
                      <MAX_SIZE>200</MAX_SIZE> <!-- Max number of JDBC connections in the pool -->
                      <TIMEOUT>500</TIMEOUT> <!-- When an idle connection is removed from the pool (seconds) -->
                      <MAX_STATEMENTS>100</MAX_STATEMENTS> <!-- Number of prepared statements will be cached. Increase performance. -->
                      <IDLE_TEST_PERIOD>3000</IDLE_TEST_PERIOD> <!-- Idle time in seconds before a connection is automatically validated. (seconds) -->
                  </C3P0>
              </HIBERNATE>
          </DB>
          <PATHS>
              <TMPFILE>~sahihi-le-scedar-api/temp/</TMPFILE>
          </PATHS>
      </API>
    ```
    - Do note the following:
        - `/API/CONTEXT/PORT` is set to **8080** (Server will serve from that port)
        - `/API/CONTEXT/PATH` is set to **/** (Server will serve from that path)
        - `/API/DB/PASSWORD_TYPE` is set to **CLEAR_TEXT**. This is important since the system will encrypt it as soon asit has read from it.
        - `/API/DB/INITIAIZE` is set to **yes**. This is important since it will tell the system to load up defaults like the ROOT user
        - `/API/DB/HIBERNATE/HBM2DDL_AUTO` is set to **create**. This is important since it will tell the system to create the tables and their properties in the DB. (It will drop them if they exist)
        - The are other value **update** will update the database and not drop existing tables. **validate** will just validate the database
        > NB: Make sure you get `/API/DB/HIBERNATE/DEFAULT_SCHEMA` and `/API/DB/HIBERNATE/DEFAULT_CATALOG` right
        > - For **MySQL**, put DEFAULT_SCHEMA as [YOUR DB NAME] and leave DEFAULT_CATALOG [BLANK]
        > - For **SQL Server**, DEFAULT_SCHEMA is mostly dbo and DEFAULT_CATALOG is mostly [YOUR DB NAME]
- Now run `-$ mvn install to install` the rest of the dependencies.
- Run `-$ mvn spring-boot:run` to run the application or `-$ mvn clean package` to build the .jar file for the application
         

